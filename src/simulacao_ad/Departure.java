package simulacao_ad;

public class Departure extends Server { //funcao de chegada
    
    public EventQueue history;
    
    public Departure(EventQueue queue, double lambda, String name, EventQueue history) {
        super(queue, lambda, name);
        this.history = history;
    }
    
    @Override
    public void run() {
        while (!super.bStop) {
            if (queue.peek() != null) {
                long time = RandomGenerator.getTime(super.lambda);
                try {
                    depart(time);
                    Thread.sleep(time/Simulador.SPEEDUP);
                } catch (InterruptedException e) {}
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {}
            }
        }
        return;
    }
   
     public void depart(long time) {//ao ser chamada adiciona para o inicio da fila um novo
        
        long totalTime = Simulador.getTime() + time;
        if(!queue.isEmpty() && totalTime <= Simulador.getMax()){
            
            Event ev = queue.get();
            ev.setDepartureTime(totalTime);
            ev.setSpentInService(time);
            //System.out.printf("Event processed at %d\n", totalTime);
            history.add(ev);
            Simulador.addSnap(queue.getSize());
        }else{
            if(totalTime > Simulador.getMax()) {
                Simulador.save(this);
                this.stopper();
                return;
            }
        }
    }
}
