package simulacao_ad;

import java.io.FileWriter;
import java.util.ArrayList;

public class Simulador {
    
    public static long totalTime;
    public static long maxTime;
    public static final int SPEEDUP = 60;
    private static ArrayList<Integer> queueSnapshots;
    public static int nDiscarded;
    private static long[] departureTL;//Todos os tempos de partida
    private static long [] arrivalTL;//Todos os tempo de chegada na fila
    private static long [] utilizationTL;//todo tempo utilizado dentro do sistema
    private static long [] waitTL;//tempo utilizado na fila e no sistema
    
    public static void main(String[] args) throws InterruptedException {
        double lambda = 0.00909090909;
        double micro = 0.01111111111111;
        if(args.length > 0) {
            System.out.println(args[0]);
            lambda = Double.parseDouble(args[0]);
        }
        queueSnapshots = new ArrayList<>();
        EventQueue history = new EventQueue();
        Simulador.totalTime = 0;
        Simulador.maxTime = 3600000;
        //Simulador.maxTime = 60000;

        EventQueue queue = new EventQueue(15);
        ArrayList<Server> servers = new ArrayList<>();
        Server s1 = new Arrival(queue, lambda, "1");
        Server s2 = new Departure(queue, micro, "2", history);
        servers.add(s1);
        servers.add(s2);
        
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                try {
                    System.out.println("Shutting down...");
                    //TODO cleaning up code...
                    for (Server server : servers) {
                        server.stopper();
                    }

                } /*catch (InterruptedException e) {
                //TODO Auto-generated catch block
                e.printStackTrace();
                System.exit(1);
            }*/ finally {
                }
            }
        });
        
        s1.start();
        s2.start();
        /*double c = 0;
        for (int i = 1; i <= 10000000; i++) {
            double x = RandomGenerator.invexp(new Double(1.0)/new Double(90.0));
            //System.out.println(x);
            c += x;
        }
        System.out.println(c/10000000);*/
        
        
        
        
        return;

    }
    
    public static long getTime() {
        return totalTime;
    }
    
    public static void incrementTime(long time) {
        totalTime += time;
    }
    
    public static long getMax() {
        return maxTime;
    }
    
    public static void addSnap(int s) {
        queueSnapshots.add(s);
    }
    
    public static void incrementDiscarded() {
        nDiscarded++;
    }
   
    public static void getResults(Departure dep){
        int length = dep.history.getSize();
        departureTL  = new long[length];
        arrivalTL = new long[length];
        utilizationTL = new long[length];
        waitTL = new long[length];
        long busyTime = 0;
        
        for (int i = length-1; i >= 0; i--) {
            long arrivalTime = dep.history.peek().getArrivalTime();
            arrivalTL[i] = arrivalTime;
            long departureTime = dep.history.peek().getDepartureTime();
            departureTL[i] = departureTime;
            //clientTL[i] = departureTL[i] - arrivalTL[i];
            utilizationTL[i] = dep.history.peek().getSpentInService();
            long waitTime = departureTime - arrivalTime;
            waitTL[i] = waitTime;
            busyTime += utilizationTL[i];
            dep.history.get(); //para tirar ele da fila
        }
        long totalWait = 0;
        for(long i : waitTL) {
            totalWait += i;
        }
        long avgWait = Math.round(new Double(totalWait) / waitTL.length);
        long avgTime = Math.round(new Double(busyTime) / utilizationTL.length);
        int avgSize = 0;
        avgSize = queueSnapshots.stream().map((i) -> i).reduce(avgSize, Integer::sum);
        avgSize = (int) Math.round(new Double(avgSize) / queueSnapshots.size());
        double discardRate = new Double (nDiscarded) / arrivalTL.length;
        double utilization = new Double(busyTime) / maxTime;
        System.out.printf("Ocupado por: %d\n", busyTime);
        System.out.printf("Descartados: %d\n", nDiscarded);
        System.out.printf("Utilização = %f\n", utilization);
        System.out.printf("E[N] = %d\n", avgSize);
        System.out.printf("E[W] = %d\n", avgWait);
        System.out.printf("Taxa de descarte = %f\n", discardRate);
    }
        
    public static void writeFiles(){
        String NEW_LINE= "\n";
        FileWriter fileWriter = null;
        String line="";
        
        String fileName = "utilizacao.csv";
        
        try{ 
           fileWriter = new FileWriter(fileName);
           
            for (int i = 0; i < utilizationTL.length; i++) {
                line = departureTL[i] + "," + utilizationTL[i];
                fileWriter.append(line);
                fileWriter.append(NEW_LINE);
            }
        } catch(Exception e) {
        }finally{
           try {
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception e) {}
        }
        
        fileName = "clienteTotal.csv";
        
        try{ 
           fileWriter = new FileWriter(fileName);
           
            for (int i = 0; i < arrivalTL.length; i++) {
                long totalTime = arrivalTL[i]+departureTL[i];
                line = departureTL[i] + "," + totalTime;
                fileWriter.append(line);
                fileWriter.append(NEW_LINE);
            }
        } catch(Exception e) {
        }finally{
           try {
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception e) {}
        }
        
        
    }
    
    public static void save(Departure dep) {
        
        getResults(dep);
        writeFiles();
        
    }
    
}
