package simulacao_ad;

/**
 *
 * @author jhln
 */
public class Synapse extends Server {
    
    public EventQueue instinct;
    public EventQueue visual;
    public EventQueue auditory;
    
    public Synapse(EventQueue queue, double lambda, String name, 
            EventQueue instinct, EventQueue visual, EventQueue auditory) {
        super(queue, lambda, name);
        this.instinct = instinct;
        this.visual = visual;
        this.auditory = auditory;
    }
    
    @Override
    public void run() {
        while (!super.bStop) {
            if (queue.peek() != null) {
                long time = RandomGenerator.getTime(super.lambda);
                try {
                    process(time);
                    Thread.sleep(time/Simulador.SPEEDUP);
                } catch (InterruptedException e) {}
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {}
            }
        }
        return;
    }
    
    public void process(long time) {//ao ser chamada adiciona para a fila correta o evento
        
        long totalTime = Simulador.getTime() + time;
        if(!queue.isEmpty() && totalTime <= Simulador.getMax()){
            
            Event ev = queue.get();
            //ev.setDepartureTime(totalTime);
            ev.setSpentInService(time);
            //System.out.printf("Event processed at %d\n", totalTime);
            switch(ev.getType()) {
                case Instinct: 
                    this.instinct.add(ev);
                    break;
                case Visual:
                    this.visual.add(ev);
                    break;
                case Auditory:
                    this.auditory.add(ev);
                    break;
            }
            Simulador.addSnap(queue.getSize());
        }else{
            if(totalTime > Simulador.getMax()) {
                //Simulador.save(this);
                this.stopper();
                return;
            }
        }
    }
    
}
