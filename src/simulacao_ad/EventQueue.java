package simulacao_ad;

import java.util.concurrent.ConcurrentLinkedQueue;

public class EventQueue {
    
    ConcurrentLinkedQueue<Event> queue;
    int capacity; //pode ser hard coded aqui para facilitar
    boolean max;
    
    public EventQueue() {
        this.queue = new ConcurrentLinkedQueue<>();
        this.capacity = 0;
        this.max = false;
    }
    
    public EventQueue(int capacity) {
        this.queue = new ConcurrentLinkedQueue<>();
        this.capacity = capacity;
        this.max = true;
    }
    
    public EventQueue(long initialTime, int capacity) {
        this.queue = new ConcurrentLinkedQueue<>();
        this.capacity = capacity;
        this.max = true;
    }
    
    public Event get() {
        return this.queue.poll();
    }
    
    public Event peek() {
        return this.queue.peek();
    }
    
    public boolean add(Event e) {
        return this.queue.add(e);
    }
    
    public boolean isFull(){
        return this.queue.size()>= capacity || !this.max;
    }
    
    public boolean isEmpty(){
        return this.queue.isEmpty();
    }
    
    public int getSize(){
        return this.queue.size();
    }
}
