package simulacao_ad;

public class RandomGenerator {
     static long  a = 0x5DEECE66DL;
     static long m = (1L << 48) - 1;
     static long c = 0xBL;
     static long seed = 364121224L;
     
     public static double rand(){
         double xi = (new Double(a)*seed + c) % m;
         double ui = xi/m;
         seed = (long) xi;
         //System.out.println(seed);
         return ui;
     } 
     
     public static double[] rand(long seed){
         double xi = (a*seed + c) % m;
         double ui = xi/m;
         double[] r  = {xi,ui};
         return r;
     }
     
     public static double invexp(double lambda) {
         double y = RandomGenerator.rand();
         //System.out.println(y);
         //System.out.println(seed);
         y = -(1/lambda)*Math.log(y);
         //System.out.println(y);
         return y;
     }
     
     public static long getTime(double lambda) {
        double t = RandomGenerator.invexp(lambda);
        //System.out.printf("t is %f\n", t);
        long ms = (long) Math.round(t);
        return ms;
     }
     
}
