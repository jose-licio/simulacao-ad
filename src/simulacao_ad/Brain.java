package simulacao_ad;

/**
 *
 * @author jhln
 */
public class Brain extends Departure {
    
    public EventQueue history;
    public Type type;
    
    public Brain(EventQueue queue, double lambda, String name, 
            EventQueue history, Type type) {
        super(queue, lambda, name, history);
        this.type = type;
    }
    
    @Override
    public void run() {
        while (!super.bStop) {
            if (queue.peek() != null) {
                long time = RandomGenerator.getTime(super.lambda);
                try {
                    think(time);
                    Thread.sleep(time/Simulador.SPEEDUP);
                } catch (InterruptedException e) {}
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {}
            }
        }
        return;
    }
   
     public void think(long time) {//ao ser chamada adiciona para o inicio da fila um novo
        
        long totalTime = Simulador.getTime() + time;
        if(!queue.isEmpty() && totalTime <= Simulador.getMax()){
            
            //Simulador.incrementTime(time);
            Event ev = queue.get();
            ev.setDepartureTime(totalTime);
            ev.setSpentInService(time);
            //System.out.printf("Event processed at %d\n", totalTime);
            history.add(ev);
            //Simulador.addSnap(queue.getSize());
        }else{
            if(totalTime > Simulador.getMax()) {
                Simulador.save(this);
                this.stopper();
                return;
            }
        }
    }
}
