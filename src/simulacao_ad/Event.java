package simulacao_ad;

public class Event {
    
    public long arrivalTime;
    public long departureTime;
    public long spentInService;
    public Type type;
    
    public Event(Type type, long time) {
        this.type = type;
        this.arrivalTime = time;
        this.departureTime = 0;
    }
    
    public void setDepartureTime(long time){
        this.departureTime = time;
    }
    
    public void setSpentInService(long time){
        this.spentInService = time;
    }
    
    public void addSpentInService(long time){
        this.spentInService += time;
    }
    
    public Type getType() {
        return this.type;
    }
    
    public long getArrivalTime() {
        return this.arrivalTime;
    }
    
    public long getDepartureTime() {
        return this.departureTime;
    }
    
    public long getSpentInService(){
        return this.spentInService;
    }
    
    public static Type randomType(){ //implementar para retornar um Type aleatorio
        double x = RandomGenerator.rand();
        if(x <= 1.0/3) {
            return Type.Instinct;
        } else if (x <= 2.0/3) {
            return Type.Visual;
        } else {
            return Type.Auditory;
        }
        
    }
    
    @Override
    public String toString() {
        return String.format("Type: %s\nArrived at %d\nDeparted at: %d\nTime spent: %d",
                "Instinct", this.arrivalTime, this.departureTime, this.spentInService);
    }
    
}
