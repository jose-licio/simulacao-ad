package simulacao_ad;

public class Server extends Thread {
    
  EventQueue queue;
  protected volatile boolean bStop;
  protected volatile boolean stopped;
  protected double lambda;
  private String name;

  public Server(EventQueue queue, double lambda, String name) {
    this.queue = queue;
    this.bStop = false;
    this.stopped = true;
    this.lambda = lambda;
    this.name = name;
  }

  @Override
  public void run() {
    this.stopped = false;
    while(!bStop) {
      //TODO server here
      //Exemplo:
      System.out.printf("Running: %s\n", this.name);
      try {
          long t = RandomGenerator.getTime(this.lambda);
          System.out.printf("Sleeping for %dms\n", t);
          Thread.sleep(t);
          
      } catch (InterruptedException e) {}
    }
    System.out.println("Exiting...");
    //Stopping code here
    this.stopped = true;
    return;
  }

  public boolean stopper() {
    this.bStop = true;
    return true;
  }

  public boolean isStopped() {
    return this.stopped;
  }
    
}
