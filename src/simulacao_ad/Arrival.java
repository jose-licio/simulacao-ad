package simulacao_ad;

public class Arrival extends Server { //servidor de chegada
    
    public Arrival(EventQueue queue, double lambda, String name) {
        super(queue, lambda, name);
    }
    
    @Override
    public void run() {
        while(!super.bStop && Simulador.getTime() < Simulador.getMax()) {
            long time = RandomGenerator.getTime(super.lambda);
            try {
                Thread.sleep(time/Simulador.SPEEDUP);
                arrive(time);
            } catch (InterruptedException e) {
                
            }
        }
        return;
    }
    
    public void arrive(long time){//ao ser chamada adiciona para o inicio da fila um novo
        
        long totalTime = Simulador.getTime() + time;
        if(!queue.isFull() && totalTime <= Simulador.getMax()){
            
            Event newEvent  = new Event(Event.randomType(), totalTime);
            Simulador.incrementTime(time);
            //System.out.printf("Event inserted at %d\n", totalTime);
            queue.add(newEvent);
        } else {
            if(totalTime > Simulador.getMax()) {
                return;
            }
            //System.out.printf("Arrival discarded because queue is full at %d\n", 
            //        Simulador.getTime() + time);
            Simulador.incrementTime(time);
            Simulador.incrementDiscarded();
        }
    }
    
}
